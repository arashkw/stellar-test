package ir.kuknos;

public class StellarApplication {
    public static void main(String[] args) {
        StellarOperations stellarOperations = new StellarOperations();
//        ir.kuknos.arash.StellarWindow window = new ir.kuknos.arash.StellarWindow();
//        window.setButtonListener((sourceAccount, targetAccount) ->
//                stellarOperations.pay(2, sourceAccount, targetAccount,
//                        window::resultMessage));
        stellarOperations.pay(2, "SC7OKURAM4AERFMLCKOPDQSYMWVYEKPP6KSXJJO4SUQRMEWKD5MPPUME",
                "GAUE6O25EEMR3P3UIDDBJULQNYZAOGI43FARBC4CI3IM6LYQJ5RDHFSQ",
                System.out::println);
    }
}
