package ir.kuknos;

import org.stellar.sdk.*;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

import java.io.IOException;

public class StellarOperations {

//    public static final String secretKey = "SCZANGBA5YHTNYVVV4C3U252E2B6P6F5T3U6MM63WBSBZATAQI3EBTQ4";
//    public static final String destination = "GA2C5RFPE6GCKMY3US5PAB6UZLKIGSPIUKSLRB6Q723BM2OARMDUYEJ5";

    private static final String STELLAR_SERVER = "https://horizon-testnet.stellar.org";
    private Server server;

    public StellarOperations() {
        server = new Server(STELLAR_SERVER);
    }

    public void pay(int fee, String secretKey, String destinationAccount,
                    StellarOperationResultListener resultListener) {

        KeyPair source = KeyPair.fromSecretSeed(secretKey);
        String destination = destinationAccount;

        AccountResponse sourceAccount = null;

        try {
            // First, check to make sure that the destination account exists.
            // You could skip this, but if the account does not exist, you will be charged
            // the transaction fee when the transaction fails.
            // It will throw HttpResponseException if account does not exist or there was another error.
            server.accounts().account(destination);

            // If there was no error, load up-to-date information on your account.
            sourceAccount = server.accounts().account(source.getAccountId());
        } catch (IOException e) {
            resultListener.resultMessage("Account does not exit");
            e.printStackTrace();
        }

        Transaction transaction = null;
        if (sourceAccount != null) {
            // Start building the transaction.
            transaction = new Transaction.Builder(sourceAccount, Network.TESTNET)
                    .addOperation(new PaymentOperation.Builder(destination, new AssetTypeNative(), String.valueOf(fee)).build())
                    // A memo allows you to add your own metadata to a transaction. It's
                    // optional and does not affect how Stellar treats the transaction.
                    .addMemo(Memo.text("Pymnt Tx"))
                    .setBaseFee(100)
                    // Wait a maximum of three minutes for the transaction
                    .setTimeout(180)
                    .build();
            // Sign the transaction to prove you are actually the person sending it.
            transaction.sign(source);
        }

        if (transaction != null)
            // And finally, send it off to Stellar!
            try {
                SubmitTransactionResponse response = server.submitTransaction(transaction);
                resultListener.resultMessage("Transaction was successful!\n"
                        + "You can check your transaction by: \n" + response.getHash());
                System.out.println("Success!");
                System.out.println(response);
            } catch (Exception e) {
                System.out.println("Something went wrong!");
                System.out.println(e.getMessage());
                resultListener.resultMessage("Error occurred, please try again");
                // If the result is unknown (no response body, timeout etc.) we simply resubmit
                // already built transaction:
                // SubmitTransactionResponse response = server.submitTransaction(transaction);
            }
    }

    public interface StellarOperationResultListener {
        void resultMessage(String message);
    }
}
