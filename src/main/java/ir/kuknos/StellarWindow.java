package ir.kuknos;

import javax.swing.*;

public class StellarWindow extends JFrame {

    private JPanel panel;
    private JTextField sourceTextField;
    private JTextField targetTextField;
    private JButton payButton;

    public StellarWindow() {
        panel = new JPanel();
        sourceAccountViews();
        destinationAccountViews();
        payButton();
        windowInitialization();
    }

    private void windowInitialization() {
        setTitle("Stellar Payment");
        setSize(500, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        add(panel);
    }

    private void sourceAccountViews() {
        JLabel label = new JLabel("Stellar SourceAccount:");
        label.setToolTipText("Enter your private key");
        sourceTextField = new JTextField();
        panel.add(label);
        panel.add(sourceTextField);
    }

    private void destinationAccountViews() {
        JLabel label = new JLabel("Stellar DestinationAccount:");
        label.setToolTipText("Enter receiver's public key");
        targetTextField = new JTextField();
        panel.add(label);
        panel.add(targetTextField);
    }

    private void payButton() {
        payButton = new JButton("PAY");
        panel.add(payButton);
    }

    public void setButtonListener(PayButtonListener listener) {
        payButton.addActionListener(e -> {
            System.out.println(sourceTextField.getText());
            System.out.println(targetTextField.getText());
            listener.getSourceAndTarget(sourceTextField.getText(), targetTextField.getText());
        });
    }

    public void resultMessage(String message) {
        JTextArea textArea = new JTextArea(message);
        panel.add(textArea);
        panel.repaint();
    }

    public interface PayButtonListener {
        void getSourceAndTarget(String sourceAccount, String targetAccount);
    }

}
